# Setting up dev environment

## Requirements

* CFX-Server
* MySQL


## Setting up Repo

Clone into dev branch:
**~ git clone -b dev https://gitlab.com/gtaodiscord/New-Dawn.git**

Edit MySQL Authentication String:
**~ nano secret.cfg**


## Setting up MySQL
* create empty database (name as specified in secret.cfg)
* run resource/[essential]/esplugin_mysql/sql.sql
* run resource/[essential]/es_extended/es_extended.sql



# Starting up Server
* Navigate to folder of repo (e.g.: **~ cd /root/New-Dawn**)
* Run run.sh from CFX-Server (e.g.: **~ bash ../cfx/run.sh +exec server.cfg**)