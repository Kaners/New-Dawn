function AddTextEntry(key, value)
	Citizen.InvokeNative(GetHashKey("ADD_TEXT_ENTRY"), key, value)
end

Citizen.CreateThread(function()
	AddTextEntry('0x0EA2DDE8', 'BMW M4 F82')  --format-- 
	
	AddTextEntry('0x000AF29F', 'Revolt S.A. Vented')

	AddTextEntry('0x006A406F', 'Green Headlight Tint')

	AddTextEntry('0x00CDF74E', 'Aluminium Drag Wing')

	AddTextEntry('0x00EE6A8D', 'OEM Grille')

	AddTextEntry('0x01AC3D24', 'Taiga Custom')

	AddTextEntry('0x01D36449', 'Elegy RH5')

	AddTextEntry('0x026ED430', 'Greenwood')

	AddTextEntry('0x02C29E2C', 'Black Wind Deflectors')

	AddTextEntry('0x03C40251', 'Annimo Bumper Bay Shore Sticker 2')

	AddTextEntry('0x03DC9555', 'Benny\'s Black')

	AddTextEntry('0x0409D787', 'Yosemite')

	AddTextEntry('0x041748C4', 'Hood Mounted Foglights')

	AddTextEntry('0x045D43C5', 'White Stripes')

	AddTextEntry('0x047559DB', 'Marabella Special')

	AddTextEntry('0x04D24E8E', 'Street Interior Streep')

	AddTextEntry('0x054672F2', 'Stripped Interior')

	AddTextEntry('0x05F7705E', 'Double Rear Exhaust')

	AddTextEntry('0x065DE338', 'Team LOMBANK')

	AddTextEntry('0x0698D87A', 'Team HFS')

	AddTextEntry('0x069DDBEC', 'Team Amigas #46')

	AddTextEntry('0x06B580EA', 'Side Dump')

	AddTextEntry('0x0819A9B9', 'Retro Racer I')

	AddTextEntry('0x0911D6F5', 'Comet Turbo Black')

	AddTextEntry('0x097FE1B0', 'Coolhand Motorsport')

	AddTextEntry('0x09C15641', 'Hizoku Custom')

	AddTextEntry('0x09FFCB9E', 'Kaminari')

	AddTextEntry('0x0A8C2C60', 'Green Japanese Plate')

	AddTextEntry('0x0A46C5BB', 'Picador Loco')

	AddTextEntry('0x0B7214C7', 'Contender Off-Road')

	AddTextEntry('0x0C2A553D', 'Race Splitter')

	AddTextEntry('0x0C86FBD0', 'Blista Compact T/A')

	AddTextEntry('0x0D0EBB9C', 'Alien Skirts')

	AddTextEntry('0x0D9200F1', 'GA Styling Double')

	AddTextEntry('0x0E5A8B9C', 'LSSD Premier')

	AddTextEntry('0x0FA69F6B', 'Bravado Racing II')

	AddTextEntry('0x0FC6CF89', 'Bobcat Dually')

	AddTextEntry('0x1A6CF1C2', 'Grachan Splitter')

	AddTextEntry('0x1A9A267F', 'Premier Classic')

	AddTextEntry('0x1B17017A', 'Elegy Wasabi')

	AddTextEntry('0x1BF24ABC', 'GTK White')

	AddTextEntry('0x1BFBE925', 'Picador Loco Drag')

	AddTextEntry('0x1C81F16F', 'Flames')

	AddTextEntry('0x1CE59EEF', 'Annimo GT2-S')

	AddTextEntry('0x1CFA0F7A', 'Stubby Duneloader')

	AddTextEntry('0x1E6A0948', 'GA Styling')

	AddTextEntry('0x1EA80C04', 'Team Terroil')

	AddTextEntry('0x1F06BFFD', 'RPD Coquette')

	AddTextEntry('0x2A9A3B18', 'Cuda Grille')

	AddTextEntry('0x2B0C4DCD', 'Gauntlet Heist')

	AddTextEntry('0x2C166F26', 'Hades')

	AddTextEntry('0x2CA0F35C', 'Front and Rear Sunstrip')

	AddTextEntry('0x2CB197CC', 'RPD Seminole')

	AddTextEntry('0x2CE7ED6B', 'Vigero Rumbler Street Rod')

	AddTextEntry('0x2CFC1ECD', 'DegenaTron Motorsports')

	AddTextEntry('0x2D09AE34', 'ES550')

	AddTextEntry('0x2D4B4E15', 'Side Mounted Straight Pipe')

	AddTextEntry('0x2D60EEDE', 'Low Level Spoiler')

	AddTextEntry('0x2D94EE01', 'GTK Grey')

	AddTextEntry('0x2D313B04', 'Elegy Retro Custom Drift Missile')

	AddTextEntry('0x2E23A793', 'Cherry Popper Japan')

	AddTextEntry('0x2E80F1E7', 'RPD Stanier')

	AddTextEntry('0x2E9155DD', 'Hizoku')

	AddTextEntry('0x2FEADDB8', 'Contender Classic')

	AddTextEntry('0x3AC5BA60', 'Cherenkov Racing')

	AddTextEntry('0x3AF16555', 'Regina X-Mas')

	AddTextEntry('0x3B014CB2', 'SAHP Bullet')

	AddTextEntry('0x3C71A48E', 'Apex Slide')

	AddTextEntry('0x3C141311', 'Sprunk Xtreme Missile')

	AddTextEntry('0x3CB545F1', 'Safari Bullbars & Lights')

	AddTextEntry('0x4A3B7708', 'Kuruma T/A')

	AddTextEntry('0x4A3BA74E', 'Annimo R-Tune Black')

	AddTextEntry('0x4AA9BB81', 'Rear Louvres')

	AddTextEntry('0x4ACFBC04', 'Fort Zancudo Camo')

	AddTextEntry('0x4BA41765', 'Bravado Special')

	AddTextEntry('0x4C3E1AF4', 'LSPD Jackal')

	AddTextEntry('0x4C649B3A', 'Patriot Beer Racing #14')

	AddTextEntry('0x4CBC60F4', 'Mesh Grille')

	AddTextEntry('0x4D4F35C2', 'FIB Faction')

	AddTextEntry('0x4DB981D5', 'Fukyu Z')

	AddTextEntry('0x4E34613D', 'Redwood Racing')

	AddTextEntry('0x5A3ABD11', 'Retro III')

	AddTextEntry('0x5B92710C', 'Bobcat XL Crew Cab Dually')

	AddTextEntry('0x5BFF2B60', 'Dual Side Mounted Exhaust')

	AddTextEntry('0x5C1B0EA9', 'Team Access')

	AddTextEntry('0x5C44CB60', 'Annimo Street Rider')

	AddTextEntry('0x5D3F3A9B', 'Classic Side Stripe')

	AddTextEntry('0x5D134AE5', 'OEM Stripes #1')

	AddTextEntry('0x5E70BF59', 'LSPD Oracle XS')

	AddTextEntry('0x5E7937CA', 'Alien Bumper Lip')

	AddTextEntry('0x5EA23FB5', 'Racing Team Solitude #02')

	AddTextEntry('0x5F2F0996', 'Faggio Delivery')

	AddTextEntry('0x5F648C1C', 'Sandking')

	AddTextEntry('0x5F9679BA', 'Custom Skirt')

	AddTextEntry('0x5FECD5D2', 'Benny\'s Originals Painted')

	AddTextEntry('0x6ACDF084', 'Ripper Roo Missile')

	AddTextEntry('0x6B769E60', 'Custom Grille')

	AddTextEntry('0x6C14D558', 'Left Mounted Front Plate')

	AddTextEntry('0x6C31EE5C', 'Holed Skirt')

	AddTextEntry('0x6CB1F7FF', 'BCSO Rancher')

	AddTextEntry('0x6CC88E36', 'Race Bumper')

	AddTextEntry('0x6CF5072F', 'Roadkiller')

	AddTextEntry('0x6D47D044', 'GTK Drag Wing')

	AddTextEntry('0x6D63DAB0', 'Boat Trailer XL')

	AddTextEntry('0x6EAFF850', 'Rust Bucket')

	AddTextEntry('0x6ED5EE6A', 'Annihilator Black')

	AddTextEntry('0x6FC190F4', 'Side Draft Carburetors')

	AddTextEntry('0x6FCEC58C', 'Sport Grille')

	AddTextEntry('0x7A15A8B9', 'Rollcage')

	AddTextEntry('0x7A5293EE', 'Dinka Power')

	AddTextEntry('0x7AFD15D2', 'Uranus')

	AddTextEntry('0x7C4F9A49', 'Elegy Plate')

	AddTextEntry('0x7C47DE83', 'Alien Bumper')

	AddTextEntry('0x7D116011', 'Mesh Grille')

	AddTextEntry('0x7DDBF9F9', 'Yosemite 6x6')

	AddTextEntry('0x7EA1B2C7', 'Classic Rear Bumper')

	AddTextEntry('0x7EC58002', 'LSPD Premier Classic')

	AddTextEntry('0x7F3415E3', 'Dukes Daytona')

	AddTextEntry('0x7FA374FB', 'Benny\'s Originals Painted')

	AddTextEntry('0x7FBB01E6', 'Annis Racing')

	AddTextEntry('0x8A45A545', 'Annihilator Blue')

	AddTextEntry('0x8AE53513', 'Xero Racing')

	AddTextEntry('0x8AEC4E46', 'Pißwasser Classic')

	AddTextEntry('0x8BF47DDC', 'Benny\'s Originals CF')

	AddTextEntry('0x8C3C134B', 'Bay Shore Sticker 1')

	AddTextEntry('0x8CA3C96C', 'Tow Hook')

	AddTextEntry('0x8CA63AE6', 'Shinigami')

	AddTextEntry('0x8D18D1C2', 'Annis Stripe')

	AddTextEntry('0x8DD6875F', 'Merryweather Security')

	AddTextEntry('0x8F4D2111', 'LSPD Solair')

	AddTextEntry('0x8F63F162', 'Lip Spoiler')

	AddTextEntry('0x8F80D236', 'Brawler Convertible')

	AddTextEntry('0x8F959D6C', 'Yosemite Dually')

	AddTextEntry('0x8FC7A1FF', 'Katana')

	AddTextEntry('0x9A38B19F', 'Remove Front Plate')

	AddTextEntry('0x9AB0B034', 'Tow Hook')

	AddTextEntry('0x9ADA294C', 'EB X-Mas Boomerang SS')

	AddTextEntry('0x9ADE8A25', 'Banger')

	AddTextEntry('0x9B076AB3', 'Street Bumper')

	AddTextEntry('0x9B9E03A3', 'Shaker Hood')

	AddTextEntry('0x9BD8FD47', 'Unmarked Coquette')

	AddTextEntry('0x9BF588C9', 'Team Eris')

	AddTextEntry('0x9BF25605', 'Gauntlet Hellhound')

	AddTextEntry('0x9CCE4E94', 'Western Zombie')

	AddTextEntry('0x9DF48A9B', 'Team Annis Motorsports')

	AddTextEntry('0x9E40451B', 'Gasser')

	AddTextEntry('0x9ECE6DC1', 'Tsurikawa')

	AddTextEntry('0x9F0C714D', 'Street Brawler Convertible')

	AddTextEntry('0x9F5B2FA2', 'Rancher 6x6')

	AddTextEntry('0x9F7E6AB2', 'Cluckin\' Bell Japan')

	AddTextEntry('0x9FCC5DE5', 'Street Splitter')

	AddTextEntry('0x11AD09D8', 'LSPD Coquette')

	AddTextEntry('0x11CC994F', 'Benny\'s Originals CF')

	AddTextEntry('0x13A507EC', 'Elegy RH5 Drift Missile')

	AddTextEntry('0x13C39A28', 'Juggernaut')

	AddTextEntry('0x14B040A8', 'Remove Plate')

	AddTextEntry('0x19CA4D1A', 'Number 9')

	AddTextEntry('0x19CDD80A', 'Mesa Renegade')

	AddTextEntry('0x23AF3D14', 'Wangan Wing')

	AddTextEntry('0x24B212BC', 'Surano GT')

	AddTextEntry('0x24D60146', 'Derelict')

	AddTextEntry('0x26A5646C', 'LSSD Bullet')

	AddTextEntry('0x27E9EAC5', 'Japanese Warrior Old And Wise')

	AddTextEntry('0x29AA35C2', 'Stripes')

	AddTextEntry('0x30AB98E9', 'Dogfighter')

	AddTextEntry('0x31EABA55', 'Team Kronos #5')

	AddTextEntry('0x33A7EA82', 'Painted Front Bumper')

	AddTextEntry('0x34CCCF66', 'X-Flow Diffuser')

	AddTextEntry('0x34E5BA6B', 'Dingo SS')

	AddTextEntry('0x35E24AE4', 'Performance Cam Cover')

	AddTextEntry('0x37F8636F', 'Side Mounted Hater Pipe')

	AddTextEntry('0x39B0EE42', 'Annimo Bumper Bay Shore Sticker 1')

	AddTextEntry('0x40BF7EA2', 'Remove Plates')

	AddTextEntry('0x43A35DC6', 'Team PostOP')

	AddTextEntry('0x43CCD9B0', 'LSSD Coquette')

	AddTextEntry('0x43F46708', 'Retro Cam Cover')

	AddTextEntry('0x46DCC54B', 'Atomic Orange')

	AddTextEntry('0x47F40685', 'OEM Spoiler Black Lip')

	AddTextEntry('0x49A1CE4D', 'Winner')

	AddTextEntry('0x51CE6EE8', 'Annis Motorsports Australia')

	AddTextEntry('0x51E3197B', 'Krieger Wing')

	AddTextEntry('0x52A406FB', 'Annimo GT2')

	AddTextEntry('0x53DCC1D3', 'Bay Shore Sticker')

	AddTextEntry('0x55A038DB', 'Safari')

	AddTextEntry('0x55A5D1B0', 'Figure8 Racing')

	AddTextEntry('0x55CB4B7A', 'Kenshin')

	AddTextEntry('0x55EB1734', 'GTK Intercooler')

	AddTextEntry('0x59B29959', 'GA Styling Single')

	AddTextEntry('0x61F64664', 'LSPD Radius')

	AddTextEntry('0x62C164F8', 'Premier Classic Taxi')

	AddTextEntry('0x62D4E744', 'RH5 Plate')

	AddTextEntry('0x62F4308D', 'Barracks Classic')

	AddTextEntry('0x64F49967', 'Yosemite Retro')

	AddTextEntry('0x65B7B163', 'Revolt S.A. Half Bumper')

	AddTextEntry('0x66A857FD', 'Rancher SUV')

	AddTextEntry('0x67F5EED0', 'Wooden Panels')

	AddTextEntry('0x72D8D05A', 'Barracks Flatbed')

	AddTextEntry('0x72F67F71', 'Zancudo Special')

	AddTextEntry('0x73E9756D', 'Chepalle Racing')

	AddTextEntry('0x73F4110E', 'Mule Flatbed')

	AddTextEntry('0x74B81526', 'Kuruma LM Race Car')

	AddTextEntry('0x75B7740A', 'Retro')

	AddTextEntry('0x75CDBA4E', 'Team Fukaru-Kansei')

	AddTextEntry('0x76A99CE2', 'Sandking Utility SWB')

	AddTextEntry('0x78B8DFCB', 'Sadler Dually')

	AddTextEntry('0x78BF6C56', 'Krieger Half Bumper')

	AddTextEntry('0x80B48A04', 'Stuntman')

	AddTextEntry('0x80F552BD', 'Team LTD')

	AddTextEntry('0x81C1039E', 'Hellhound Classic')

	AddTextEntry('0x83D00277', 'Benny\'s Originals CF')

	AddTextEntry('0x85E5509E', 'Comet Turbo White')

	AddTextEntry('0x88BA3060', 'The Lost')

	AddTextEntry('0x90DD87AE', 'Alien Bumper CF Lip')

	AddTextEntry('0x92B1F308', 'Taped Headlights')

	AddTextEntry('0x92C309EF', 'Double Side Exhaust')

	AddTextEntry('0x93AA2FEF', 'Dirt Trial Gauntlet')

	AddTextEntry('0x94AF777C', 'Shakotan Exhaust')

	AddTextEntry('0x94BA14A4', 'Black Cambelt Covers')

	AddTextEntry('0x98A5FC96', 'Limitless Street Drag')

	AddTextEntry('0x98FB4971', 'Dukes Mayhem')

	AddTextEntry('0x99B6EBDB', 'Chief')

	AddTextEntry('0x99E4262F', 'OEM Base Bonnet')

	AddTextEntry('0x140D35B6', 'Benny\'s White')

	AddTextEntry('0x173C68E7', 'Caracal')

	AddTextEntry('0x209F415C', 'Coolhand Racing')

	AddTextEntry('0x229BB8B6', 'Dual Side Dump')

	AddTextEntry('0x287C4BD9', 'Revolt S.A. Lights')

	AddTextEntry('0x295DBE34', 'LSPD Pony')

	AddTextEntry('0x322A71F0', 'Benny\'s Gold')

	AddTextEntry('0x341CE857', 'Bravado Racing RTR')

	AddTextEntry('0x376E54EC', 'PCPD Vigero Rumbler')

	AddTextEntry('0x387E03D3', 'Annimo R-Tune White')

	AddTextEntry('0x405DC590', 'Pfister Red')

	AddTextEntry('0x412BFD30', 'Bay Shore Sticker 2')

	AddTextEntry('0x434C1757', 'OEM Stripe #2')

	AddTextEntry('0x475F1485', 'SAHP Faction')

	AddTextEntry('0x488B19B2', 'Retro IV')

	AddTextEntry('0x491D61AD', 'Yosemite Muscle Truck')

	AddTextEntry('0x491D292A', 'SAHP Coquette')

	AddTextEntry('0x499F6159', 'Twin Exhaust')

	AddTextEntry('0x501AC93C', 'Mule Recovery')

	AddTextEntry('0x527DEB23', 'Yosemite Ramp Truck')

	AddTextEntry('0x579FBA3B', 'Street Fenders')

	AddTextEntry('0x582F67F0', 'Kaido Racer')

	AddTextEntry('0x640CD0B5', 'Retro II')

	AddTextEntry('0x642C0D2C', 'Comet Turbo Red')

	AddTextEntry('0x654A2107', 'Team Meinmacht')

	AddTextEntry('0x673BC94F', 'Black Bumper Lip')

	AddTextEntry('0x676D753F', 'Figure8 Ricer')

	AddTextEntry('0x688B4BDE', 'Annimo Bumper Painted Lip')

	AddTextEntry('0x702AB128', 'Team GTK')

	AddTextEntry('0x730CE01F', 'Issi Hardtop')

	AddTextEntry('0x734C5E50', 'Gauntlet Classic')

	AddTextEntry('0x744EC93E', 'Elegy RH5 Pace Car')

	AddTextEntry('0x783A8347', 'GTK Black')

	AddTextEntry('0x786A018E', 'OEM Stripe #3')

	AddTextEntry('0x810BF300', 'Rancher XL SUV')

	AddTextEntry('0x830AB1CB', 'GT Stealth')

	AddTextEntry('0x841BC5B0', 'Executioner')

	AddTextEntry('0x841CF397', 'WAVE Intercooler')

	AddTextEntry('0x857B214F', 'Flash')

	AddTextEntry('0x872D2352', 'Pegassi FCR 1000')

	AddTextEntry('0x918AF40C', 'Benny\'s Originals')

	AddTextEntry('0x936B4062', 'Benny\'s Black')

	AddTextEntry('0x946E7544', 'Performance Hood')

	AddTextEntry('0x958E60A4', 'Painted Rear Bumper')

	AddTextEntry('0x962F2072', 'Freedom Fighter')

	AddTextEntry('0x991EFC04', 'Comet Retro')

	AddTextEntry('0x1388A1DA', 'Annimo Bumper Black Lip')

	AddTextEntry('0x1635C007', 'Vigero Rumbler')

	AddTextEntry('0x1933B04A', 'Contender Classic Beater')

	AddTextEntry('0x2290C50A', 'Warrener Coupe')

	AddTextEntry('0x2645C241', 'Remove Spoiler')

	AddTextEntry('0x2747E5D7', 'Front Left Plate')

	AddTextEntry('0x3687B1F8', 'Touring Car Chassis')

	AddTextEntry('0x4651E97B', 'Yankee')

	AddTextEntry('0x5158F165', 'Elegy Group A Test Car')

	AddTextEntry('0x5530F7E6', 'Primary Headlight Covers')

	AddTextEntry('0x5635A308', 'Small Spoiler')

	AddTextEntry('0x6196F49F', 'Twin Turbo')

	AddTextEntry('0x6251C714', 'Endo Engineering')

	AddTextEntry('0x7032F7AC', 'Front Right Plate')

	AddTextEntry('0x7245A95F', 'Pfister Black')

	AddTextEntry('0x7980BDD5', 'Euros')

	AddTextEntry('0x8454D9A0', 'Team Inkei')

	AddTextEntry('0x8526E2F5', 'Slamvan Classic')

	AddTextEntry('0x9134CE1F', 'Team GTK')

	AddTextEntry('0x9382CB60', 'Secondary Light Covers')

	AddTextEntry('0x9592A5FC', 'Benny\'s Originals Painted')

	AddTextEntry('0x9992F4FA', 'USAF Uranus')

	AddTextEntry('0x17626E5F', 'Headlight Covers')

	AddTextEntry('0x27563A11', 'Fieldmaster Custom')

	AddTextEntry('0x29095FAE', 'Retro Racer')

	AddTextEntry('0x30745BC1', 'RR Lip')

	AddTextEntry('0x42876ADB', 'Bay Shore Sticker')

	AddTextEntry('0x51253BCF', 'Benny\'s Gold')

	AddTextEntry('0x55012C6B', 'Annimo R-Tune')

	AddTextEntry('0x62602B23', 'Coyote Off-Road')

	AddTextEntry('0x63739B4C', 'Rusty Skull')

	AddTextEntry('0x65149CC4', 'Mid Level Spoiler')

	AddTextEntry('0x66533B00', 'Sadler Retro')

	AddTextEntry('0x78589AD7', 'Rebel 4x4')

	AddTextEntry('0x87965A77', 'Bay Shore Wave Special')

	AddTextEntry('0x91812B99', 'Night Rider')

	AddTextEntry('0x182458BD', 'BCSO Stanier')

	AddTextEntry('0x187989CA', 'Car Trailer')

	AddTextEntry('0x393518CF', 'Fort Zancudo Plain')

	AddTextEntry('0x400261B8', 'Annimo GT1')

	AddTextEntry('0x433588D4', 'Your\'s Custom')

	AddTextEntry('0x442256F8', 'Pegasus Racing')

	AddTextEntry('0x531047CB', 'Nakazato')

	AddTextEntry('0x574631BE', 'Classic Front Bumper')

	AddTextEntry('0x737471E4', 'Race Fenders')

	AddTextEntry('0x821138C7', 'Custom Bumper')

	AddTextEntry('0x890424DA', 'Your\'s Custom')

	AddTextEntry('0x954802C6', 'Stockman')

	AddTextEntry('0x983086DF', 'Yeti Clothing #22')

	AddTextEntry('0x990413BA', 'Suzuka')

	AddTextEntry('0x6487817D', '550CUI')

	AddTextEntry('0x6620978C', 'Limitless JGTC')

	AddTextEntry('0x7030607D', 'Bravado Racing I')

	AddTextEntry('0x8467385D', 'Sandking Utility Single Cab')

	AddTextEntry('0x9734012D', 'Rumpo Escape')

	AddTextEntry('0x12754260', 'Black Valve Covers')

	AddTextEntry('0x14982409', 'Rally Lights')

	AddTextEntry('0x30449084', 'Bobcat')

	AddTextEntry('0x46459787', 'Turbo Hare Fenders')

	AddTextEntry('0x55421151', 'Emperor Limousine Monster')

	AddTextEntry('0x66100607', 'Bobcat XL Dually')

	AddTextEntry('0x80390985', 'Injection Raid')

	AddTextEntry('0x90488660', 'Alien Garage Diffuser')

	AddTextEntry('0x90775164', 'Futo JTCC')

	AddTextEntry('0xA2BF139B', 'Dual Shotgun Exhaust')

	AddTextEntry('0xA3BCD833', 'Annihilator White')

	AddTextEntry('0xA4D568F3', 'FF-Works')

	AddTextEntry('0xA5A0E4CD', 'Benny\'s White')

	AddTextEntry('0xA8F9BAA5', 'Grille Eyelids')

	AddTextEntry('0xA34EFE2E', 'Annis Racing')

	AddTextEntry('0xA79BB0E9', 'Tsukuba')

	AddTextEntry('0xA5473687', 'Ducktail Spoiler')

	AddTextEntry('0xAA55571A', 'Mega Midget Mouse #83')

	AddTextEntry('0xAC38DB00', 'Sandking XL Dually')

	AddTextEntry('0xAD4C5050', 'Alien Wing')

	AddTextEntry('0xAD701280', 'Kabuki')

	AddTextEntry('0xAE0CFA8A', 'Stock Car Splitter')

	AddTextEntry('0xAF2CE6F4', 'Retro V')

	AddTextEntry('0xAF2F2F50', 'Team Fuji Racing')

	AddTextEntry('0xAFC62DA9', 'Tuner Exhaust')

	AddTextEntry('0xAFD78F39', 'Sandking Utility')

	AddTextEntry('0xAFDDDA92', 'Revolt S.A.')

	AddTextEntry('0xB01C9061', 'Sturdy Bumper')

	AddTextEntry('0xB09DD4A4', 'Side Mounted Exhaust')

	AddTextEntry('0xB0CBD05D', 'eCola Racing')

	AddTextEntry('0xB6A18018', 'RH5 Black')

	AddTextEntry('0xB6C01C9B', 'LSPD Infernus')

	AddTextEntry('0xB6CEBFA3', 'Double Side Exhaust')

	AddTextEntry('0xB8B7E07C', 'Regina Sedan')

	AddTextEntry('0xB50FEC96', 'Unmarked Pony')

	AddTextEntry('0xB81AF24C', 'Primo Beater')

	AddTextEntry('0xB95D546C', 'Elegy OEM')

	AddTextEntry('0xB954B300', 'Unmarked Infernus')

	AddTextEntry('0xB2971D9B', 'Melvin Harris Racing')

	AddTextEntry('0xB3814CF5', 'Revolt S.A. Half Bumper Finned')

	AddTextEntry('0xB5817BBC', 'Globe Oil')

	AddTextEntry('0xB2068867', 'Primary Light Covers')

	AddTextEntry('0xBB905FD2', 'GTK Grille Cover')

	AddTextEntry('0xBB62324A', 'Rear Louvers')

	AddTextEntry('0xBC41832E', 'Rancher Pickup')

	AddTextEntry('0xBD52C8C2', 'Big Bore Exhaust')

	AddTextEntry('0xBD79CBE5', 'Team Annis-Air Herler')

	AddTextEntry('0xBD2907EB', 'Turbo R Black')

	AddTextEntry('0xBF7BF4AF', 'Zero-Yon Wing')

	AddTextEntry('0xBFAC442B', 'Pfister White')

	AddTextEntry('0xBFACBEDD', 'Alien Headlights')

	AddTextEntry('0xBFC9510E', 'Monster ANNIS Retro Intercooler')

	AddTextEntry('0xC0615740', 'Team Brand 69')

	AddTextEntry('0xC3EF6990', 'Nakazato')

	AddTextEntry('0xC4C0AF88', 'Primary Bed')

	AddTextEntry('0xC4FE46E6', 'Vintage Splitter')

	AddTextEntry('0xC8B0117F', 'Beater')

	AddTextEntry('0xC11BE82A', 'Alien Bumper CF')

	AddTextEntry('0xC52C6B93', 'Dominator Classic')

	AddTextEntry('0xC53CABF9', 'Annimo Skirts')

	AddTextEntry('0xC68EFBE3', 'eCola Racing (Worn)')

	AddTextEntry('0xC72B99AF', 'Emperor Meth')

	AddTextEntry('0xC93BFDDA', 'Sabre Lowrider')

	AddTextEntry('0xC94C5234', 'Yellow Headlight Tint')

	AddTextEntry('0xC98BBAD6', 'Glendale Coupe')

	AddTextEntry('0xC308FAB6', 'Dirt Jumper')

	AddTextEntry('0xC7401F39', 'Annihilator Red')

	AddTextEntry('0xC8995EB5', 'Mesh Headlight Cover')

	AddTextEntry('0xC18879A8', 'OEM Spoiler')

	AddTextEntry('0xC172330C', 'Custom Bumper')

	AddTextEntry('0xCAA98B06', 'CF Drag Wing')

	AddTextEntry('0xCB60E7B3', 'Takeshi Nakazato')

	AddTextEntry('0xCB75C6CB', 'Sturdy')

	AddTextEntry('0xCCBA221E', 'Requiem')

	AddTextEntry('0xCCC30751', 'OEM Antenna 2')

	AddTextEntry('0xCCD6FA55', 'TMPD Elegy Retro')

	AddTextEntry('0xCCD78DC2', 'TMPD Battle Damaged (Lightbar)')

	AddTextEntry('0xCD0C6E80', 'Hellhound Classic (Beater)')

	AddTextEntry('0xCD995AB6', 'Taped Headlights')

	AddTextEntry('0xCE87E85D', 'Tenshun Motorsports')

	AddTextEntry('0xCEF09ED3', 'Sultan RS CT')

	AddTextEntry('0xD054CEE4', 'FIB Stanier')

	AddTextEntry('0xD3CD5C42', 'Sturdy Bumper')

	AddTextEntry('0xD3DB7539', 'Seconday Headlight Covers')

	AddTextEntry('0xD6FCA760', 'Longpath')

	AddTextEntry('0xD8F53D42', 'Emperor Snapped Turbo')

	AddTextEntry('0xD28A73C6', 'Team Amigas #71')

	AddTextEntry('0xD76CE746', 'Beater')

	AddTextEntry('0xD88D3389', 'Pipman Racing Club #76')

	AddTextEntry('0xD91FA00A', 'Touring Car Antenna')

	AddTextEntry('0xD187A040', 'Right Mounted Front Plate')

	AddTextEntry('0xD312BC24', 'Benny\'s Originals CF')

	AddTextEntry('0xD999FAA5', 'Classic Grille')

	AddTextEntry('0xD2834B0D', 'Secondary Bed')

	AddTextEntry('0xD3464AB4', 'Bobcat Security')

	AddTextEntry('0xD4644F37', 'Gruppe Sechs')

	AddTextEntry('0xD5947CA4', 'ANNIS Retro Intercooler')

	AddTextEntry('0xD8428F68', 'SAEMS Emergency Utility Truck')

	AddTextEntry('0xD31687DA', 'Fuji LM')

	AddTextEntry('0xD750946F', 'GTK Diffuser')

	AddTextEntry('0xD5369932', 'Charles Williams Racing')

	AddTextEntry('0xDA42C61A', 'Speedo Coroner')

	AddTextEntry('0xDAE169F6', 'LSPD Bullet')

	AddTextEntry('0xDB9F4296', 'Emperor Snapped')

	AddTextEntry('0xDC8B3A08', 'Sultan RS Ute')

	AddTextEntry('0xDE3EB757', 'RR Lip Bay Shore')

	AddTextEntry('0xDE10D366', 'Black Japanese Plate')

	AddTextEntry('0xDECA0511', 'Black Grille')

	AddTextEntry('0xDED61429', 'Team Kronos #1')

	AddTextEntry('0xDF7DE803', 'SAHP Uranus')

	AddTextEntry('0xE0097FAE', 'Blue Headlight Tint')

	AddTextEntry('0xE03BCE10', 'Turbo R Special')

	AddTextEntry('0xE050AC49', 'Burrito Custom')

	AddTextEntry('0xE0C1F10D', 'Sadler Retro Utility')

	AddTextEntry('0xE2FC1141', 'Ripper Roo Drift-Spec')

	AddTextEntry('0xE6C8486A', 'GTK Drag Bumper')

	AddTextEntry('0xE6D3E3A6', 'GA Styling')

	AddTextEntry('0xE6E967F8', 'Patriot Classic')

	AddTextEntry('0xE6F23CA9', 'E&L')

	AddTextEntry('0xE6F348BC', 'Alien Bumper')

	AddTextEntry('0xE7D09E52', 'Team Fukaru')

	AddTextEntry('0xE8B44050', 'LSSD Rancher')

	AddTextEntry('0xE8F27654', 'Mesa SWB')

	AddTextEntry('0xE13A81F8', 'Full Light Cover')

	AddTextEntry('0xE425EFFA', 'Streiter Wagon')

	AddTextEntry('0xE503B466', 'Ardent Aqua')

	AddTextEntry('0xE7561E20', 'Yosemite SUV')

	AddTextEntry('0xE37879E1', 'LSPD Pinnacle')

	AddTextEntry('0xE71233F2', 'Benny\'s Originals Painted')

	AddTextEntry('0xE86144D5', 'ANNIMO Worn')

	AddTextEntry('0xE932095D', 'Fugitive SS')

	AddTextEntry('0xEA4DF61B', 'Revolt S.A. Skirts')

	AddTextEntry('0xEA82E29E', 'Turbo R White')

	AddTextEntry('0xEA828CA4', 'Kabuki Classic')

	AddTextEntry('0xEAC32C03', 'Yeti Clothing #29')

	AddTextEntry('0xEAF81686', 'Grille Mounted Foglights')

	AddTextEntry('0xEC00A4BA', 'Front Splitter')

	AddTextEntry('0xECC1937C', 'Bobcat XL Crew Cab')

	AddTextEntry('0xEE2E3B1C', 'Hood Pins')

	AddTextEntry('0xEE7F67A9', 'Rancher Stepside')

	AddTextEntry('0xEE25B548', 'Coyote')

	AddTextEntry('0xEE76AE68', 'ANNIMO Intercooler')

	AddTextEntry('0xEF106ED9', 'Land Dogfighter')

	AddTextEntry('0xEFC5FE21', 'BCSO Premier')

	AddTextEntry('0xEFD29C86', 'Street Splitter')

	AddTextEntry('0xF021C0E6', 'TMPD Elegy RH5')

	AddTextEntry('0xF5C62D3E', 'LSPD Uranus')

	AddTextEntry('0xF6E6BEBE', 'Not Tonight PizzaBoy')

	AddTextEntry('0xF8AEEC33', 'GA Styling')

	AddTextEntry('0xF8EF0794', 'Dingo')

	AddTextEntry('0xF9BF0E70', 'JDM AF')

	AddTextEntry('0xF9DC3879', 'SAFD Emergency Utility Truck')

	AddTextEntry('0xF12AB6C0', 'BMX Custom')

	AddTextEntry('0xF29EA4D8', 'Red Headlight Tint')

	AddTextEntry('0xF55AB5FE', 'Sprunk Extreme Drift-Spec')

	AddTextEntry('0xF64CFF6E', 'RH5 White')

	AddTextEntry('0xF85C3F69', 'Team Zenshin Motorsport')

	AddTextEntry('0xF89FEC19', 'Drag Covered Intakes')

	AddTextEntry('0xF120FBC8', 'Tofu Delivery')

	AddTextEntry('0xF641AC06', 'Revolt S.A. Headlights')

	AddTextEntry('0xF648A36C', 'Rollcage & Chassis Upgrade')

	AddTextEntry('0xF684BC7D', 'Yosemite SWB')

	AddTextEntry('0xF713DA23', 'Race Spoiler')

	AddTextEntry('0xF831D4D3', 'Quad Rear Exhaust')

	AddTextEntry('0xF861B2B5', 'SAHP Infernus')

	AddTextEntry('0xF955ADB6', 'Cherry')

	AddTextEntry('0xF2498AF9', 'Lex Is a Ricer')

	AddTextEntry('0xF4839FE2', 'Rollcage & Logo')

	AddTextEntry('0xF54596D2', 'Touring Car Rollcage')

	AddTextEntry('0xF68107EC', 'Alexi Drift Missile')

	AddTextEntry('0xF440915E', 'Kart')

	AddTextEntry('0xF6914038', 'TMPD Battle Damaged')

	AddTextEntry('0xFA1DCEC0', 'Armordillo')

	AddTextEntry('0xFA95B170', 'Fender Vents')

	AddTextEntry('0xFA378868', 'Mud Flaps')

	AddTextEntry('0xFA836919', '1999 Racing Team Solitude Retired')

	AddTextEntry('0xFB67E594', 'RON Racing')

	AddTextEntry('0xFBA86638', 'LSSD Interceptor')

	AddTextEntry('0xFBAE2EFB', 'Mid Night')

	AddTextEntry('0xFC0647B7', 'Remove Passenger Seat')

	AddTextEntry('0xFC4DCF18', 'Team Annis')

	AddTextEntry('0xFC56DA5A', 'Fuji LMGT')

	AddTextEntry('0xFC409207', 'Retro Racer II')

	AddTextEntry('0xFDA56915', 'OEM Antenna 1')

	AddTextEntry('0xFDE6FBEC', 'Bravado Racing TA')

	AddTextEntry('0xFE40D0E8', 'Classic Side Stripe (Beater)')

	AddTextEntry('0xFEF11F61', 'Alien Skirts Lip')

	AddTextEntry('0xFF708701', 'Yosemite XL SUV')

end)
