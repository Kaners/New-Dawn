-- CONFIG --

-- The watermark text --
text1 = "New-Dawn"
text2 = "https://discord.gg/gtao"

-- The x and y offset (starting at the top left corner) --
-- Default: 0.005, 0.001
offset1 = {x = 0.005, y = 0.001}
offset2 = {x = 0.005, y = 0.045}

-- Text RGB Color --
-- Default: 64, 64, 64 (gray)
rgb1 = {r = 54, g = 57, b = 62}
rgb2 = {r = 108, g = 128, b = 204}

-- Text transparency --
-- Default: 255
alpha = 150

-- Text scale
-- Default: 0.4
-- NOTE: Number needs to be a float (so instead of 1 do 1.0)
scale1 = 1.0
scale2 = 0.4

-- Text Font --
-- 0 - 5 possible
-- Default: 1
font = 1

-- Rainbow Text --
-- false: Turn off
-- true: Activate rainbow text (overrides color)
bringontherainbows = false

-- CODE --
Citizen.CreateThread(function()
	while true do
		Wait(1)

		if bringontherainbows then
			rgb = RGBRainbow(1)
		end

		SetTextFont(font)
		SetTextWrap(0.0, 1.0)
		SetTextCentre(false)
		SetTextDropshadow(2, 2, 0, 0, 0)
		SetTextEdge(1, 0, 0, 0, 205)
    SetTextEntry("STRING")

		SetTextColour(rgb1.r, rgb1.g, rgb1.b, alpha)
    SetTextScale(scale1, scale1)
		AddTextComponentString(text1)
		DrawText(offset1.x, offset1.y)


		SetTextFont(font)
		SetTextWrap(0.0, 1.0)
		SetTextCentre(false)
		SetTextDropshadow(2, 2, 0, 0, 0)
		SetTextEdge(1, 0, 0, 0, 205)
		SetTextEntry("STRING")
		SetTextColour(rgb2.r, rgb2.g, rgb2.b, alpha)
    SetTextScale(scale2, scale2)
		AddTextComponentString(text2)
		DrawText(offset2.x, offset2.y)
	end
end)

-- By ash
function RGBRainbow(frequency)
    local result = {}
    local curtime = GetGameTimer() / 1000

    result.r = math.floor(math.sin(curtime * frequency + 0) * 127 + 128)
    result.g = math.floor(math.sin(curtime * frequency + 2) * 127 + 128)
    result.b = math.floor(math.sin(curtime * frequency + 4) * 127 + 128)

    return result
end
