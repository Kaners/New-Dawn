ZEUS = ZEUS or {}
ZEUS.Permissions = {}

ZEUS.Permissions.getDefaultGroup = function()
  for k,v in ipairs(ZEUS.Config.Permissions) do
    if v[default] then
      return k
    end
  end
end

ZEUS.Permissions.doesUserHavePermission = function(identifier, permission, cb)
  if not group or not ZEUS.Config.Permissions[group] then
    group = ZEUS.Permissions.getDefaultGroup()
  end
  ZEUS.PlayerData.getData(identifier, "group", function(group)
      local value = ZEUS.Permissions.doesGroupHavePermission(group, permission)
      cb(value)
  end)
end

ZEUS.Permissions.doesGroupHavePermission = function(group, permission)
  if ZEUS.Config.Permissions[group].deny then
    for _,v in pairs(ZEUS.Config.Permissions[group].deny) do
      if string.match(v, permission) ~= nil then
        return false
      end
    end
  end

  if ZEUS.Config.Permissions[group].allow then
    for _,v in pairs(ZEUS.Config.Permissions[group].allow) do
      if string.match(v, permission) ~= nil then
        return true
      end
    end
  end

  if ZEUS.Config.Permissions[group].inherits then
    local inheritGroup = ZEUS.Config.Permissions[ZEUS.Config.Permissions[group].inherits]
    if inheritGroup then
      if type(ZEUS.Config.Permissions[group].inherits) == "table" then
        for k,v in ipairs(ZEUS.Config.Permissions[group].inherits) do
          local value = ZEUS.Permissions.doesGroupHavePermission(ZEUS.Config.permissions[group].inherits, permission)
          if value then
            return true
          end
        end
      elseif type(ZEUS.Config.Permissions[group].inherits) == "string" then
        local value = ZEUS.Permissions.doesGroupHavePermission(ZEUS.Config.Permissions[group].inherits, permission)
        if value then
          return true
        end
      else
        RconPrint("ZEUS: INHERITS FIELD IN CONFIG FOR GROUP '"..group.."' IS OF INVALID TYPE")
      end
    else
      RconPrint("ZEUS: COULD NOT FIND GROUP '"..ZEUS.Config.Permissions[group].inherits.."' for inheritance.\n")
    end
  end
  return false
end
