local messages = {
    'Message ^1one',
    'Message ^2two',
    'Message ^3three',
    'Message ^4four',
    'Message ^5five',
    'Message ^6six',
}

Citizen.CreateThread(function()
  local messageCycle = 1

  while true do
    Wait(GetConvar("ANNOUNCER_INTERVALTIME", 10)*1000*60)

    if GetConvar("ANNOUNCER_ENABLED", 1) then
      local players = GetPlayers()
      for k,v in ipairs(players) do
        local prefix = GetConvar("ANNOUNCER_PREFIX", "[Announcer]")
        local suffix = GetConvar("ANNOUNCER_SUFFIX", "")
        TriggerClientEvent("chatMessage", v, '', {255, 255, 255}, prefix .. " " .. messages[messageCycle] .. suffix)
      end

      messageCycle = messageCycle + 1
      if messageCycle > #messages then
        messageCycle = 1
      end
    end
  end
end)
