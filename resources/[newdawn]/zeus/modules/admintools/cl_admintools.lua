RegisterNetEvent('admintools:spawnVehicle')
AddEventHandler('admintools:spawnVehicle', function(v)
  local hash = GetHashKey(v)

  RequestModel(hash)

  while not HasModelLoaded(hash) do
  	RequestModel(hash)
  	Citizen.Wait(0)
  end

  local x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), false))
  local vehicle = CreateVehicle(hash, x + 2, y + 2, z + 1, 0.0, true, false)
  SetEntityAsMissionEntity(vehicle, true, true)

  TriggerEvent('chatMessage', 'ZEUS', {0, 255, 0}, 'Vehicle spawned successfully!')
end)
