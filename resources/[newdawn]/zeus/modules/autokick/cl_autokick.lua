-- AFK-Kick
RegisterNetEvent("zeus:autokick_afk")
AddEventHandler("zeus:autokick_afk", function()
  print("received server event")

  local playerPed = GetPlayerPed(-1)
  if playerPed then
    playerPosition = GetEntityCoords(playerPed, true)

    print("send server event")
    TriggerServerEvent("zeus:autokick_afk_resp", playerPosition.x, playerPosition.y, playerPosition.z)
  end
end)
