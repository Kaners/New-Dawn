-- Ping-Kick
Citizen.CreateThread(function()
  local pingHistory = {}
  local pingCycle = 1

	while true do
		Wait(GetConvar("AUTOKICK_PINGINTERVAL", 5)*1000*60)

    local players = GetPlayers()
    for k,v in ipairs(players) do
      local maxPing = GetConvar('AUTOKICK_MAXPING', 400)
      if maxPing == -1 then
        break
      end

      local kickReason = GetConvar("AUTOKICK_KICKREASON_PING", "Kicked for to high latency.")
      if not pingHistory[v] then
        pingHistory[v] = {}
      end
      pingHistory[v][pingCycle] = GetPlayerPing(v)

      local average = 0
      for k,v in ipairs(pingHistory[v]) do
        average = average + v
      end
      average = average / #pingHistory[v]

      if average > tonumber(maxPing) then
        DropPlayer(v, kickReason)
      end
    end

    pingCycle = pingCycle + 1
    if pingCycle > 5 then
      pingCycle = 1
    end
	end
end)




-- AFK-Kick
Citizen.CreateThread(function()
  while true do
    Wait(GetConvar("AUTOKICK_MAXAFK", 15)*1000*60)

    local players = GetPlayers()
    for k,v in ipairs(players) do
      TriggerClientEvent("zeus:autokick_afk", v)
    end
  end
end)

local lastPositions = {}
RegisterServerEvent("zeus:autokick_afk_resp")
AddEventHandler("zeus:autokick_afk_resp", function(x, y, z)
  local pos1 = {x=x, y=y, z=z}
  local pos2 = lastPositions[source]
  if pos2 then
    if pos1.x == pos2.x and pos1.y == pos2.y and pos1.z == pos2.z then
      local kickReason = GetConvar("AUTOKICK_KICKREASON_AFK", "Kicked for being AFK for to long.")
      DropPlayer(source, kickReason)
    end
  end

  lastPositions[source] = pos1
end)
