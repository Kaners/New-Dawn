local addPlayer = function(identifier, cb)
  MySQL.Async.execute("INSERT INTO playerdata (identifier) VALUES(@identifier)", {identifier=identifier}, function()
    cb()
  end)
end
local checkPlayerExists = function(identifier, cb)
  MySQL.Async.fetchAll("SELECT identifier FROM playerdata WHERE identifier=@identifier", {identifier=identifier}, function(res)
    if res[1] then
      cb(true)
    else
      cb(false)
    end
  end)
end
local addColumn = function(columnName, cb)
  MySQL.Async.execute("ALTER TABLE playerdata ADD `".. columnName .."` TEXT;", {}, function()
    cb()
  end)
end
local checkColumnExists = function(columnName, cb)
  MySQL.Async.fetchAll("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='essentialmode' AND `TABLE_NAME`='playerdata';", {}, function(res)
    if res then
      for k,v in ipairs(res) do
        if v["COLUMN_NAME"] == columnName then
          cb(true)
          return
        end
      end
      cb(false)
    end
  end)
end


ZEUS = ZEUS or {}
ZEUS.PlayerData = {}

ZEUS.PlayerData.getData = function(identifier, key, cb)
  checkColumnExists(key, function(columnExists)
    if columnExists then
      MySQL.Async.fetchAll("SELECT `".. key .."` FROM playerdata WHERE identifier=@identifier;", {key=key,identifier=identifier}, function(res)
        if res[1] then
          cb(res[1][key])
        else
          cb(false)
        end
      end)
    else
      cb(false)
    end
  end)
end
ZEUS.PlayerData.setData = function(identifier, key, value, cb)
  local mysql = function()
    MySQL.Async.execute("UPDATE playerdata SET `".. key .."`=@value WHERE identifier=@identifier;", {value=value,identifier=identifier}, function()
      cb()
    end)
  end
  local helper = function()
    checkColumnExists(key, function(res)
      if res then
        mysql()
      else
        addColumn(key, function()
          mysql()
        end)
      end
    end)
  end

  ZEUS.PlayerData.getData(identifier, "identifier", function(playerExists)
    if playerExists then
      helper()
    else
      addPlayer(identifier, function()
        helper()
      end)
    end
  end)
end
