ZEUS = ZEUS or {}
ZEUS.Config = {}

ZEUS.Config.Permissions = {
  user = {
    default = true,

    allow = {
      "command.test1",
      "command.test2",
      "command.test3",
      "command.test4",
    },
  },
  moderator = {
    inherits = "user",

    allow = {
      "command.test5",
      "command.test6",
    },

    deny = {
      "command.test4",
    },
  },
  admin = {
    inherits = "moderator",

    allow = {
    },

    deny = {

    }
  },
  superadmin = {
    inherits = "admin",

    deny = {
      "command.*",
    }

  },
}
