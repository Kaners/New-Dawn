local addData = function(key, value, cb)
  MySQL.Async.execute("INSERT INTO globaldata (`key`, value) VALUES(@key, @value)", {key=key,value=value}, function()
    cb()
  end)
end

--[[
local checkDataExists = function(key, cb)
  MySQL.Async.fetchAll("SELECT `key` FROM globaldata WHERE `key`=@key", {key=key}, function(res)
    if res[1] then
      cb(true)
    else
      cb(false)
    end
  end)
end
]]--

ZEUS = ZEUS or {}
ZEUS.GlobalData = {}

ZEUS.GlobalData.getData = function(key, cb)
  MySQL.Async.fetchAll("SELECT value FROM globaldata WHERE `key`=@key", {key=key}, function(res)
    if res[1] then
      cb(res[1]["value"])
    else
      cb(false)
    end
  end)
end
ZEUS.GlobalData.setData = function(key, value, cb)
  ZEUS.GlobalData.getData(key, function(dataExists)
    if dataExists then
      MySQL.Async.execute("UPDATE globaldata SET value=@value WHERE `key`=@key", {key=key,value=value}, function()
        cb()
      end)
    else
      addData(key, value, function()
        cb()
      end)
    end
  end)
end
