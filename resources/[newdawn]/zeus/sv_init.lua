-- /playerdata 1 money 100
-- /playerdata 1 money
TriggerEvent('es:addCommand', 'playerdata', function(source, args, user)
  if #args >= 2 then
    identifier = ZEUS.Util.getIdentifierById(args[1])

    if args[3] then
      ZEUS.PlayerData.setData(identifier, args[2], args[3], function()
        TriggerClientEvent("zeus:chatMessage", source, "Player data '".. args[2] .."' set to '".. args[3] .."'!")
      end)
    else
      ZEUS.PlayerData.getData(identifier, args[2], function(val)
        TriggerClientEvent("zeus:chatMessage", source, "Player data for key '".. args[2] .."': ".. tostring(val))
      end)
    end

  end
end, {help = "Manipulates PlayerData"})

-- /globaldata welcomeText HelloWorld
-- /globaldata welcomeText
TriggerEvent('es:addCommand', 'globaldata', function(source, args, user)
  if #args >= 1 then

    if args[2] then
      ZEUS.GlobalData.setData(args[1], args[2], function()
        TriggerClientEvent("zeus:chatMessage", source, "Global data '".. args[1] .."' set to '".. args[2] .."'!")
      end)
    else
      ZEUS.GlobalData.getData(args[1], function(val)
        TriggerClientEvent("zeus:chatMessage", source, "Global data for key '".. args[1] .."': ".. tostring(val))
      end)
    end

  end
end, {help = "Manipulates GlobalData"})


-- /zeus
TriggerEvent('es:addCommand', 'zeus', function(source, args, user)
  if #args == 0 then
    TriggerClientEvent("zeus:chatMessage", source, "This server is running ZEUS v" .. ZEUS.version);
  end
end, {help = "Prints out ZEUS Version Number"})


-- kick PlayerID REASON
TriggerEvent('es:addCommand', 'kick', function(source, args, user)
  DropPlayer(args[1], args[2])
end, {help = "Kicks the specified player"})


-- slay PlayerID
TriggerEvent('es:addCommand', 'slay', function(source, args, user)
  local ped = GetPlayerPed(args[1])
  if ped then
    SetEntityHealth(ped, 0)
  else
    TriggerClientEvent("zeus:chatMessage", source, "Could not find player-ped");
  end
end, {help = "Slays the specified player"})


-- ban
TriggerEvent('es:addCommand', 'ban', function(source, args, user)
end, {help = "Bans the specified player permanently"})

-- ping
TriggerEvent('es:addCommand', 'ping', function(source, args, user)
  TriggerClientEvent("zeus:chatMessage", source, "Your Ping: " .. GetPlayerPing(source));
end, {help = "Displays ping from client to server"})
