resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

server_scripts {
	"@mysql-async/lib/MySQL.lua",
	"sv_config.lua",

	"sh_init.lua",
	"sv_util.lua",
	"sv_init.lua",

	"sv_playerdata.lua",
	"sv_globaldata.lua",

	"sv_permissions.lua",
	"sv_commands.lua",

	"modules/admintools/sv_admintools.lua",
	"modules/autokick/sv_autokick.lua",
	"modules/announcer/sv_announcer.lua",
}
client_scripts {
	"sh_init.lua",
	"cl_init.lua",

	"modules/admintools/cl_admintools.lua",
	"modules/autokick/cl_autokick.lua",
	"modules/announcer/cl_announcer.lua",
}
