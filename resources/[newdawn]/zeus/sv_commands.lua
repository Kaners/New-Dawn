ZEUS = ZEUS or {}

local commands = {}

RegisterServerEvent('zeus:playerLoaded')
AddEventHandler("zeus:playerLoaded", function()
  for k,v in pairs(commands) do
    TriggerClientEvent('chat:addSuggestion', source, "/" .. k, v.help, v.params)
  end
end)


ZEUS.registerCommand = function(command, cb, suggestion)
  commands[command] = {}
	commands[command].cb = cb

  if not suggestion.params or not type(suggestion.params) == "table" then suggestion.params = {} end
  if not suggestion.help or not type(suggestion.help) == "string" then suggestion.help = "" end
  commands[command].suggestion = suggestion

  RegisterCommand(command, function(source, args)
    local identifier = ZEUS.Util.getIdentifierById(source)
      ZEUS.Permissions.doesUserHavePermission(identifier, "command." .. command, function(doesHavePermission)
      if doesHavePermission then
        cb(source, args)
      else
        TriggerClientEvent("zeus:chatMessage", source, "You do not have access to that command.")
      end
    end)
	end, false)
end
