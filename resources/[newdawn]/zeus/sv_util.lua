ZEUS = ZEUS or {}
ZEUS.Util = {}

ZEUS.Util.getIdentifierById = function(id)
  local identifier
  for k,v in ipairs(GetPlayerIdentifiers(id))do
    if string.sub(v, 1, string.len("license:")) == "license:" then
      identifier = v
      break
    end
  end

  return identifier
end

ZEUS.Util.printTable = function(x)
  local firstRun = true

  RconPrint("{")
  for k,v in ipairs(x) do
    if not firstRun then
      RconPrint(", ")
    else
      firstRun = false
    end
    RconPrint(k .. " => " .. v)
  end
  RconPrint("}\n")
end

ZEUS.Util.tableContains = function(haystack, needle)
  for _,v in pairs(haystack) do
    if v == needle then
      return true
    end
  end
  return false
end
