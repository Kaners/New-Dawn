resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
	'data-main/vehicles.meta',
	'data-main/carcols.meta',
	'data-main/carvariations.meta',
	'data-dispatch/dispatch.meta',
	'data-dispatch/popcycle.dat',
	'data-dispatch/vehiclemodelsets.meta',
	'data-dispatch/popgroups.ymt'
}

data_file 'VEHICLE_METADATA_FILE' 'data-main/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data-main/carvariations.meta'
data_file 'CARCOLS_FILE' 'data-main/carcols.meta'
data_file 'TEXTFILE_METAFILE' 'data-dispatch/dispatch.meta'
data_file 'VEHICLE_MODELSETS_FILE' 'data-dispatch/vehiclemodelsets.meta'
data_file 'POPSCHED_FILE' 'data-dispatch/popcycle.dat'
data_file 'FIVEM_LOVES_YOU_341B23A2F0E0F131' 'data-dispatch/popgroups.ymt'
